-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Client :  localhost:3306
-- Généré le :  Jeu 12 Septembre 2019 à 16:34
-- Version du serveur :  5.7.27-0ubuntu0.18.04.1
-- Version de PHP :  7.2.19-0ubuntu0.18.04.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `Semaine 11  Nodejs et BDD`
--

-- --------------------------------------------------------

--
-- Structure de la table `Articles`
--
-- Création :  Jeu 12 Septembre 2019 à 11:46
-- Dernière modification :  Jeu 12 Septembre 2019 à 11:46
--

CREATE TABLE `Articles` (
  `id` int(11) NOT NULL,
  `slug` varchar(60) NOT NULL,
  `Titre` varchar(255) NOT NULL,
  `Texte_paragraphe` longtext NOT NULL,
  `Date_du_post` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Date du poste uniquement ne prend pas en compte la date d''une modification',
  `fk_utilisateurs` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `Articles`
--

INSERT INTO `Articles` (`id`, `slug`, `Titre`, `Texte_paragraphe`, `Date_du_post`, `fk_utilisateurs`) VALUES
(1, 'mon_article', 'Mon article !', 'bla bla bla', '2019-09-11 14:29:43', 3);

-- --------------------------------------------------------

--
-- Structure de la table `Commentaires`
--
-- Création :  Jeu 12 Septembre 2019 à 13:06
--

CREATE TABLE `Commentaires` (
  `id` int(11) NOT NULL,
  `Date_de_post` datetime NOT NULL,
  `Paragraphe` text NOT NULL,
  `Date_de_modification` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fk_articles` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `Utilisateurs`
--
-- Création :  Mer 11 Septembre 2019 à 07:17
-- Dernière modification :  Jeu 12 Septembre 2019 à 08:01
--

CREATE TABLE `Utilisateurs` (
  `id` int(11) NOT NULL,
  `Nom` varchar(255) NOT NULL,
  `Prénom` varchar(255) NOT NULL,
  `Mail` varchar(255) NOT NULL,
  `Mot_de_passe` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `Utilisateurs`
--

INSERT INTO `Utilisateurs` (`id`, `Nom`, `Prénom`, `Mail`, `Mot_de_passe`) VALUES
(1, 'Chibaco', 'Ridhoine', 'ridhoine@live.fr', 'ABC123'),
(2, 'Maxime', 'DO', 'maxigo@live.fr', 'AZERTY'),
(3, 'Dimitri', 'Klopfstein', 'dimitri.kft@gmail.com', 'WXcv'),
(4, 'Thomas', 'Revel', 'magmix31@gmail.com', 'PPOK12a'),
(5, 'Lucas', 'Chevalier', 'lucaschev17@gmail.com', 'qster19'),
(6, 'Marie', 'Capelle', 'marie.capelle03@gmail.com', 'CapMell27'),
(8, 'Sarkozy', 'Django', 'ok@gmail.com', 'azerty');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `Articles`
--
ALTER TABLE `Articles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `slug` (`slug`),
  ADD KEY `fk_utilisateurs` (`fk_utilisateurs`);

--
-- Index pour la table `Commentaires`
--
ALTER TABLE `Commentaires`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Index pour la table `Utilisateurs`
--
ALTER TABLE `Utilisateurs`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `Articles`
--
ALTER TABLE `Articles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `Commentaires`
--
ALTER TABLE `Commentaires`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `Utilisateurs`
--
ALTER TABLE `Utilisateurs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `Articles`
--
ALTER TABLE `Articles`
  ADD CONSTRAINT `Articles_ibfk_1` FOREIGN KEY (`fk_utilisateurs`) REFERENCES `Utilisateurs` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
