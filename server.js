// APPEL DES PAQUET NPM
const express    = require('express');
const mysql      = require('mysql');
const bodyParser = require('body-parser');

// PARAMETRAGE DES MIDDLEWARE
const app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(express.static('public'));




// ROUTES A DEFINIR
app.get('/register', function(req, res) {
    res.sendFile(__dirname + '/public/register.html');
});

app.get('/login', function(req, res) {
    res.sendFile(__dirname + '/public/login.html');
});

app.get('/create', function(req, res) {
    res.send('Création d\'article');
});

// CREATION DE LA CONNECTION BDD
var con = mysql.createConnection({
    host: "localhost",
    user: "root",

   

    password: "",
    database: "blog"

  });
  
// CONNECTION BDD
  con.connect(function(err) {
    if (err) throw err;
    console.log("Vous êtes connecté a vos base de données mysql");
  });

  // VALIDATION FORMULAIRE
app.post('/newregister', function(req, res) {
  console.log(req.body);
  let prenom = req.body.prenom;
  let nom    = req.body.nom;
  let mail   = req.body.mail;
  let mdp    = req.body.password;
  let sql    = (`INSERT       INTO Utilisateurs (Prénom, Nom, Mail, Mot_de_passe) VALUES ("${prenom}", "${nom}", "${mail}","${mdp}");`);
  let query  = con.query(sql, (err, result) => {
        if(err) throw err;
        console.log(result);
        });
  res.redirect('/');
});

// CONNECTION AU BLOG
app.post('/connexion/', function(req, res) {
    let mail  =  req.body.mail;
    let mdp   =  req.body.password;
    con.query('SELECT *     FROM Utilisateurs WHERE Mail = "'+ mail +'"&&Mot_de_passe="'+mdp+'"',(err, result, fields) => {
    let data  =  result;
    console.log(data);
    if(data.length <1){
      res.redirect('/register.html');
    }
    else {
      res.redirect('/index.html');
    }
   } );
});

// //CREATION DE DATA BASE
// app.get('/createdb', (req, res) => {
//     let sql = 'CREATE DATABASE node_test';
//     con.query(sql, (err, result) => {
//         if(err) throw err;
//         console.log(result);
//         res.send('Database created...');
//     })
// }) 

// // CREATION DE TABLE
// app.get('/createpoststable', (req, res)=> {
//     let sql = 'CREATE TABLE posts(id int AUTO_INCREMENT, title VARCHAR(255), body VARCHAR(255), PRIMARY KEY (id))';
//     con.query(sql, (err, result) => {
//         if(err) throw err;
//         console.log(result);
//         res.send('Table posts crée ...');
//     });
// });

//INSERT POST 1, INSERTION DU PREMIER POST
app.get("/news", (req, res) => {
    let utilisateur = {Nom:'Sarkozy', Prénom:'Django', Mail:'ok@gmail.com', Mot_de_passe :'azerty'};
    let sql = 'INSERT INTO Utilisateurs SET ?';
    let query = con.query(sql, utilisateur, (err, result) => {
        if(err) throw err;
        console.log(result);
        res.send('second poste ajouté ...')
    })
});

// // SELECTION DES POSTS
// app.get("/getposts", (req, res) => {
//     let sql = 'SELECT * FROM posts';
//     let query = con.query(sql, (err, result) => {
//         if(err) throw err;
//         console.log(result);
//         res.send('Va chercher le post...')
//     })
// });

// // SELECTION D'UN POST
// app.get("/getpost/:id", (req, res) => {
//     let sql = `SELECT * FROM posts WHERE id = ${req.params.id}`;
//     let query = con.query(sql, (err, result) => {
//         if(err) throw err;
//         console.log(result);
//         res.send('Va chercher le post...')
//     })
// });

// MISE A JOUR "UPDATE" D'UN POST
// app.get("/updatepost/:id", (req, res) => {
//     let newTitle = 'Updated Title';
//     let sql = `UPDATE posts SET title = '${newTitle}' WHERE id = ${req.params.id}`;
//     let query = con.query(sql, (err, result) => {
//         if(err) throw err;
//         console.log(result);
//         res.send('Titre du post mise à jour...')
//     })
// });

// SUPPRESSION D'UN POST
// app.get("/deletepost/:id", (req, res) => {
//     let sql = `DELETE FROM posts WHERE id = ${req.params.id}`;
//     let query = con.query(sql, (err, result) => {
//         if(err) throw err;
//         console.log(result);
//         res.send('Le post à bien été supprimé...')
//     })
// });


// CONNECTION AU SERVEUR
app.listen(3000, function () {
    console.log('Vous êtes arrimé au port 3000')
  })
  